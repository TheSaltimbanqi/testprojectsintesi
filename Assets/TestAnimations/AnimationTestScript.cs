﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationTestScript : MonoBehaviour {

    public Animator m_Anim;
    void Start ()
    {
        this.GetComponent<Animator>();
	}
	
	void Update ()
    {
		if(Input.GetKeyDown("1"))
        {
            m_Anim.SetBool("isAttacking", true);

            m_Anim.SetBool("isWalking", false);
            m_Anim.SetBool("getDamage", false);
            m_Anim.SetBool("isAttackB", false);
            m_Anim.SetBool("isDeath", false);
            m_Anim.SetBool("isDeathB", false);
        }
        if (Input.GetKeyDown("2"))
        {
            m_Anim.SetBool("isWalking", true);

            m_Anim.SetBool("isAttacking", false);            
            m_Anim.SetBool("getDamage", false);
            m_Anim.SetBool("isAttackB", false);
            m_Anim.SetBool("isDeath", false);
            m_Anim.SetBool("isDeathB", false);
        }
        if (Input.GetKeyDown("3"))
        {
            m_Anim.SetBool("getDamage", true);

            m_Anim.SetBool("isAttacking", false);
            m_Anim.SetBool("isWalking", false);
            m_Anim.SetBool("isAttackB", false);
            m_Anim.SetBool("isDeath", false);
            m_Anim.SetBool("isDeathB", false);
        }
        if (Input.GetKeyDown("4"))
        {
            m_Anim.SetBool("isAttackB", true);

            m_Anim.SetBool("isAttacking", false);
            m_Anim.SetBool("isWalking", false);
            m_Anim.SetBool("getDamage", false);
            m_Anim.SetBool("isDeath", false);
            m_Anim.SetBool("isDeathB", false);
        }
        if (Input.GetKeyDown("5"))
        {
            m_Anim.SetBool("isDeath", true);

            m_Anim.SetBool("isAttacking", false);
            m_Anim.SetBool("isWalking", false);
            m_Anim.SetBool("getDamage", false);
            m_Anim.SetBool("isAttackB", false);
            m_Anim.SetBool("isDeathB", false);
        }
        if (Input.GetKeyDown("6"))
        {          
            m_Anim.SetBool("isDeathB", true);

            m_Anim.SetBool("isAttacking", false);
            m_Anim.SetBool("isWalking", false);
            m_Anim.SetBool("getDamage", false);
            m_Anim.SetBool("isAttackB", false);
            m_Anim.SetBool("isDeath", false);
        }
    }
}
