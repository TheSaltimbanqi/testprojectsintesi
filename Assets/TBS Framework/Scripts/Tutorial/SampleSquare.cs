﻿using System.Collections.Generic;
using UnityEngine;

class SampleSquare : Square
{
    public override Vector3 GetCellDimensions()
    {
        return GetComponent<Renderer>().bounds.size;
    }

    public override void MarkAsHighlighted()
    {
        //GetComponent<Renderer>().material.color = new Color(0.75f, 0.75f, 0.75f);
        GetComponent<Renderer>().material.color = Color.red;
        /*
        for (int i=0;i<CellNeighbous.Count;++i)
        {
            CellNeighbous[i].GetComponent<Renderer>().material.color = Color.gray;
        }
        */
    }

    public override void MarkAsPath()
    {
        GetComponent<Renderer>().material.color = Color.green;
    }

    public override void MarkAsReachable()
    {
        GetComponent<Renderer>().material.color = Color.yellow;
    }

    public override void UnMark()
    {
        GetComponent<Renderer>().material.color = Color.white;
        /*
        for (int i = 0; i < CellNeighbous.Count; ++i)
        {
            CellNeighbous[i].GetComponent<Renderer>().material.color = Color.white;
        }
        */
    }
}

