﻿Shader "TextureCombine_One_Drawcall" {
	Properties{
		_MainTex("Base (RGB)", 2D) = "white" {}
	_SecondTex("Base (RGB)", 2D) = "white" {}
	}
		SubShader{
		Pass{
		// Apply base texture
		SetTexture[_MainTex]{
		combine texture
	}

		// Blend in the _SecondTex texture using the lerp operator
		SetTexture[_SecondTex]{
		combine texture lerp(texture) previous
	}
	}
	}

		Fallback "VertexLit"
}