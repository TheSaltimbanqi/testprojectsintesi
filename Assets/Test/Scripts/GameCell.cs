﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using System;

public abstract class GameCell : MonoBehaviour, INodeGraph, IEquatable<GameCell>
{
    [SerializeField]
    private Vector2 _offSetCoord;

    public void SetOffSetCoord(int _x, int _y)
    {
        _offSetCoord = new Vector2(_x, _y);
    }
    public Vector2 GetOffsetCoord()
    {
        return _offSetCoord;
    }

    public bool m_IsTaken;
    public int m_MovementCost;

    public abstract List<GameCell> GetGameCellNeighbours(List<GameCell> _cell);

    public abstract Vector3 GetGameCellDimensions();

    public abstract int GetDistance(GameCell _block);

    public abstract void MarkAsReachable();

    public abstract void MarkAsPath();

    public abstract void MarkAsHighlighted();

    public abstract void UnMark();

    public int GetDistance(INodeGraph _block)
    {
        return GetDistance(_block as GameCell);
    }
    
    public bool Equals(GameCell _block)
    {
        return (GetOffsetCoord().x == _block.GetOffsetCoord().x && GetOffsetCoord().y == _block.GetOffsetCoord().y); 
    }

    public override bool Equals(object other)
    {
        if(!(other is GameCell))
        {
            return false;
        }
        return Equals(other as GameCell);
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }
}
