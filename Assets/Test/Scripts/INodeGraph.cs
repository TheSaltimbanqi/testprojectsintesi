﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface INodeGraph
{
    int GetDistance(INodeGraph _block);
}
