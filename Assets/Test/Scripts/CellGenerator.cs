﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellGenerator : MonoBehaviour {
    [Header("Cell Prefab")]
    public GameObject m_Prefab;
    [Header("Grid Parent Object")]
    public Transform m_Parent;
    [Header("Grid Sizes")]
    public int m_Width;
    public int m_Height;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
