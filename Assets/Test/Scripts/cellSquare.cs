﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cellSquare : GameCell {
    
    public Color m_Color;

    private Renderer m_Render;

    List<GameCell> m_Neighbours = null;


    void Start ()
    {
        m_Render=this.gameObject.GetComponent<Renderer>();
        m_Render.material.color = Color.white;
	}
	
	void Update ()
    {
		
	}
    
    protected virtual void OnMouseEnter()
    {
        m_Render.material.color = Color.yellow;
        Debug.Log(gameObject.name);
        
    }
    protected virtual void OnMouseExit()
    {
        m_Render.material.color = Color.white;
    }

    protected static readonly Vector2[] gameDirections =
    {
        new Vector2(1,0), new Vector2(-1,0), new Vector2(0,1), new Vector2(0,-1)
    };

    public override List<GameCell> GetGameCellNeighbours(List<GameCell> _cell)
    {
        if(m_Neighbours==null)
        {
            m_Neighbours = new List<GameCell>(4);
            foreach(var l_Direction in gameDirections)
            {
                var l_Neighbour = _cell.Find(c => c.GetOffsetCoord() == GetOffsetCoord() + l_Direction);
                if (l_Neighbour == null)
                    continue;
                m_Neighbours.Add(l_Neighbour);
                l_Neighbour.GetComponent<Renderer>().material.color = Color.yellow;
            }
        }
        return m_Neighbours;
    }

    public override Vector3 GetGameCellDimensions()
    {
        return GetComponent<Renderer>().bounds.size;
    }

    public override int GetDistance(GameCell _block)
    {
        return (int)(Mathf.Abs(GetOffsetCoord().x - _block.GetOffsetCoord().x) + Mathf.Abs(GetOffsetCoord().y - _block.GetOffsetCoord().y));
    }

    public override void MarkAsReachable()
    {
        GetComponent<Renderer>().material.color = Color.yellow;
    }

    public override void MarkAsPath()
    {
        GetComponent<Renderer>().material.color = Color.green;
    }

    public override void MarkAsHighlighted()
    {
        GetComponent<Renderer>().material.color = Color.grey;
    }

    public override void UnMark()
    {
        GetComponent<Renderer>().material.color = Color.white;
    }
}
