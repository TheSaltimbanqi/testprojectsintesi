﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

[ExecuteInEditMode()]
[CustomEditor(typeof(CellGenerator))]
public class CellGeneratorEditor : Editor {

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        CellGenerator l_CellGen = (CellGenerator)target;

        if (GUILayout.Button("Generate grid"))
        {
            GenerateGrid(l_CellGen);
        }
        
        if (GUILayout.Button("Clear grid"))
        {
            RemoveGrid(l_CellGen);
        }

    }

    public List<GameCell> GenerateGrid(CellGenerator _generator)
    {
        List<GameCell> l_CellGrid = new List<GameCell>();
        Debug.Log("Width: "+_generator.m_Width+" - Height: "+_generator.m_Height+" - TOTAL: "+(_generator.m_Width*_generator.m_Height));
        
        for (int i=0;i<_generator.m_Height;++i)
        {
            for(int j=0;j<_generator.m_Width;++j)
            {
                Debug.Log(j + " - " + i);
                GameObject l_Cell = Instantiate(_generator.m_Prefab);
                l_Cell.transform.position = new Vector3(j,0,i);
                l_Cell.transform.parent=_generator.m_Parent.transform;
            }
        }
        return l_CellGrid;
    }
    
    void RemoveGrid(CellGenerator _generator)
    {
        Transform[] l_Children = _generator.gameObject.GetComponentsInChildren<Transform>();
        foreach (Transform l_Child in l_Children)
        {
            if (l_Child.name == "cellSquare(Clone)")
            {
                DestroyImmediate(l_Child.gameObject);
            }            
        }
    }

    //CODE NO UTIL
    /*
    public List<Cell> GenerateCellGrid(CellGenerator _generator)
    {
        List<Cell> l_CellGrid = new List<Cell>();
        Debug.Log("Width: " + _generator.m_Width + " - Height: " + _generator.m_Height + " - TOTAL: " + (_generator.m_Width * _generator.m_Height));

        if (_generator.m_Prefab.GetComponent<Square>() == null)
        {
            Debug.LogError("Invalid square cell prefab provided");
            return l_CellGrid;
        }

        for (int i = 0; i < _generator.m_Height; ++i)
        {
            for (int j = 0; j < _generator.m_Width; ++j)
            {
                var l_Square = Instantiate(_generator.m_Prefab);
                var l_SquareSize = l_Square.GetComponent<Cell>().GetCellDimensions();

                l_Square.transform.position = new Vector3(i * l_SquareSize.x, 0, j * l_SquareSize.z);
                l_Square.GetComponent<Cell>().OffsetCoord = new Vector2(j, i);
                l_Square.GetComponent<Cell>().MovementCost = 1;
                l_CellGrid.Add(l_Square.GetComponent<Cell>());

                l_Square.transform.parent = _generator.m_Parent;
            }
        }
        return l_CellGrid;
    }
    */
}
#endif