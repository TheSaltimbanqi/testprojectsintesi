﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameGrid : MonoBehaviour {


    public List<GameCell> m_Cells { get; private set; }

    // Use this for initialization
    void Start ()
    {
        InitGame();
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    private void InitGame()
    {
        //Players

        //Cells
        m_Cells = new List<GameCell>();
        for (int i = 0; i < transform.childCount; ++i)
        {
            var l_Cell = transform.GetChild(i).gameObject.GetComponent<GameCell>();
            if (l_Cell != null)
                m_Cells.Add(l_Cell);
            else
                Debug.LogError("Invalid objkect in cells parent game object");
        }
        
        foreach(var l_Cell in m_Cells)
        {
            //l_Cell.CellClicked += OnCellClicked;
            //l_Cell.CellHighlighted += OnCellHighlighted;
            //l_Cell.CellDehighlighted += OnCellDehighlighted;
            l_Cell.GetComponent<GameCell>().GetGameCellNeighbours(m_Cells);
        }
    }

    private void OnGameCellHiglighted(object _sender, EventArgs e)
    {

    }
}
